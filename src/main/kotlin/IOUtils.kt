import java.io.EOFException
import java.io.File
import java.io.IOException
import java.io.OutputStream
import java.util.concurrent.CyclicBarrier
import java.util.concurrent.Executors.newFixedThreadPool
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.atomic.AtomicBoolean

fun <T : OutputStream> T.suppressClose() = object : OutputStream() {
    override fun write(b: Int) = this@suppressClose.write(b)
    override fun write(b: ByteArray?) = this@suppressClose.write(b)
    override fun write(b: ByteArray?, off: Int, len: Int) = this@suppressClose.write(b, off, len)
    override fun flush() = this@suppressClose.flush()
}

fun File.deleteIfExists() {
    if (exists() && !delete()) throw IOException("Failed to delete file $this")
}

/**
 * Output stream which dispatch operations to 1, or more underlying streams. Dispatching takes place
 * a concurrent manner, but all calling parties will always return the calling thread.
 */
@Suppress("MemberVisibilityCanBePrivate")
class ParallelOutputStream(target: OutputStream, vararg moreTargets: OutputStream) : OutputStream() {

    private val targets = arrayOf(target) + moreTargets
    private val available = AtomicBoolean(true)
    private val barrier = CyclicBarrier(targets.size)
    private val processors = newFixedThreadPool(targets.size).also { (it as ThreadPoolExecutor).corePoolSize = targets.size }

    var ignoreIOExceptionOnClosing: Boolean = true

    private fun outputWith(eofAction: Boolean = false, action: OutputStream.() -> Unit) {

        if (!eofAction && eof) {
            throw EOFException()
        }

        targets.forEach { output ->
            processors.submit {
                try {
                    output.action()
                } catch (e: IOException) {
                    if (!eofAction || !ignoreIOExceptionOnClosing) {
                        throw e
                    }
                } finally {
                    barrier.await()
                }
            }
        }

        if (eofAction && !eof) {
            processors.shutdown()
            available.set(false)
        }
    }

    @Suppress("MemberVisibilityCanBePrivate")
    val eof: Boolean
        get() = !available.get()

    override fun write(byte: Int) = outputWith { write(byte) }

    override fun write(bytes: ByteArray, off: Int, len: Int) {

        if (off < 0 || off >= bytes.size || (off + len) > bytes.size) {
            throw IndexOutOfBoundsException("Offset($off), and len($len) does not fit into length of 0 up to ${bytes.size - 1}")
        }

        outputWith { write(bytes, off, len) }
    }

    override fun write(byte: ByteArray) = write(byte, 0, byte.size)
    override fun close() = outputWith(eofAction = true) { close() }
    override fun flush() = outputWith { flush() }

}




