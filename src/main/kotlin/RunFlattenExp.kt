@file:JvmName("RunFlattenExp")

import java.io.File
import java.io.PrintWriter
import java.util.*
import kotlin.NoSuchElementException


private fun flattenWithRecursion(source: List<Any?>, dest: MutableList<Int>) {
    source.forEach { a ->
        when (a) {
            is Int -> dest += a
            is List<Any?> -> flattenWithRecursion(a, dest)
            else -> throw IllegalArgumentException("$source should only contain ints, or list of ints.")
        }
    }
}

private fun flattenWithOptRecursion1(source: List<Any?>, dest: MutableList<Int>) {

    fun flatten(a: Any?) {

        if (a == null) {
            return
        }

        if (a is Int) {
            dest += a
            return
        }

        if (a is List<Any?>) {
            for (x in a) {
                flatten(x)
            }
        }
    }

    flatten(source)
}


private fun flattenWithOptRecursion2(source: List<Any?>, dest: MutableList<Int>) {

    fun flatten(a: Any?) {
        when (a) {
            null -> return
            is Int -> dest += a
            is List<Any?> -> a.forEach(::flatten)
        }
    }

    flatten(source)
}

private fun flattenViaInternalStack(source: List<Any?>, dest: MutableList<Int>) {

    val pending = LinkedList<Any?>().apply { add(source) }

    while (pending.isNotEmpty()) {
        val any = pending.removeAt(0)
        when (any) {
            is Int -> dest += any
            is List<Any?> -> any.filterNotNullTo(pending)
            else -> throw IllegalArgumentException("$source should only contain ints, or list of ints.")
        }
    }
}

private fun buildInFlatten(source: List<Any?>, dest: MutableList<Int>) {
    source.flatMapTo(dest) { any ->
        when (any) {
            is Int -> sequenceOf(any).asIterable()
            is List<Any?> -> any.asSequence().filterIsInstance<Int>().asIterable()
            else -> throw IllegalArgumentException("Only expected list of ints, or ints in $any")
        }
    }
}

fun List<Any>.flattenToInts(flatten: (source: List<Any?>, dest: MutableList<Int>) -> Unit): List<Int> = mutableListOf<Int>().also { dest -> flatten(this, dest) }.toList()


fun main(args: Array<String>) {

    val sources = mapOf(
            "1" to listOf(1, 2, 3, listOf(4, 5, 6), listOf(7, listOf(9, 10, 11))),
            "2" to listOf(10, 11, 12, listOf(9, 10, 11, listOf(19, 120, 1, listOf(10, 10)))),
            "3" to listOf(828181, 8, 7, 2),
            "4" to listOf((1 until 1000).toList())
    )

    val source = { sources.pickAnyValue() }

    val uom = CallDuration.UOM.NANO_SECONDS
    val stdout = System.out.suppressClose()
    val numOfIterations = 1000
    val resultsFile = File("resultsFile.txt").apply { deleteIfExists() }

    PrintWriter(ParallelOutputStream(stdout, resultsFile.outputStream())).use { out ->

        (0 until numOfIterations).forEach {

            val flattenWithIntsMeasurement = "recursion " to CallDuration.measure(uom) { source().flattenToInts(::flattenWithRecursion) }
            val flattenWithOwnStackMeasurement = "own stack " to CallDuration.measure(uom) { source().flattenToInts(::flattenViaInternalStack) }
            val flattenWithBuiltIn = "built-in " to CallDuration.measure(uom) { source().flattenToInts(::buildInFlatten) }
            val flattenWithRecursive1 = "inner recursive function 1 " to CallDuration.measure(uom) { source().flattenToInts(::flattenWithOptRecursion1) }
            val flattenWithInnerRecursive2 = "inner recursive function 2 " to CallDuration.measure(uom) { source().flattenToInts(::flattenWithOptRecursion2) }
            val flattenWithJavaImplWithOwnStack = "java impl with own stack " to CallDuration.measure(uom) { source().flattenToInts(JavaFlattenExp::flattenWithOwnStack) }
            val flattenWithJavaImplRecursive = "java impl with recursive " to CallDuration.measure(uom) { source().flattenToInts(JavaFlattenExp::flattenRecursive) }

            val experiments = sequenceOf(
                    flattenWithIntsMeasurement,
                    flattenWithOwnStackMeasurement,
                    flattenWithBuiltIn,
                    flattenWithRecursive1,
                    flattenWithInnerRecursive2,
                    flattenWithJavaImplWithOwnStack,
                    flattenWithJavaImplRecursive)
                    .sortedBy { (_, measurement) -> measurement.value }
                    .map { (case, m) -> case to m.copy(uom = CallDuration.UOM.MILLI_SECONDS, value = m.value / 1000) }
                    .toMap()


            out.println("----------------------------------------------------------------------------------------------------------------------------------------")

            for ((testCase, measurement) in experiments) {
                out.println("$testCase : $measurement")
            }
        }
    }

}

private val random = Random()

private fun <K,V> Map<K,V>.pickAnyValue():V {
    if (isEmpty()) throw NoSuchElementException("Cannot pick any value from empty map.")
    val depth = random.nextInt(size)
    return keys.asSequence().drop(depth).first().let(this::get)!!
}

