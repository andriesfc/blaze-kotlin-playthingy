import com.fasterxml.jackson.annotation.JsonFormat
import java.time.LocalDateTime
import kotlin.system.measureNanoTime
import kotlin.system.measureTimeMillis

object CallDuration {

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    enum class UOM {
        NANO_SECONDS,
        MILLI_SECONDS
    }

    override fun toString(): String = "CallDuration"

    fun <T> measure(uom: UOM = UOM.NANO_SECONDS, action: () -> T): Measurement<CallDuration, Double, UOM, T> {
        val now = LocalDateTime.now()
        var captured: T? = null
        val dt = when (uom) {
            UOM.NANO_SECONDS -> measureNanoTime { captured = action() }
            UOM.MILLI_SECONDS -> measureTimeMillis { captured = action() }
        }
        return Measurement(CallDuration, dt.toDouble(), uom, captured!!, now)
    }
}