import java.time.LocalDateTime

data class Measurement<M, T, U, C>(
        val topic: M,
        val value: T,
        val uom: U,
        val captured: C,
        val timestamp: LocalDateTime = LocalDateTime.now()
)


