
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static java.util.Collections.unmodifiableList;

public class Lists {

    private static List<Integer> flattenInts(List<?> source) {

        LinkedList<Object> stack = new LinkedList<>();
        stack.push(source);
        ArrayList<Integer> collected = new ArrayList<>(source.size());

        while (!stack.isEmpty()) {

            Object a = stack.pop();

            if (a instanceof List<?>) {
                int n = ((List) a).size();
                int i = n;
                while (i > 0) stack.push(((List) a).get(--i));
            } else if (a instanceof Integer) {
                collected.add((Integer) a);
            } else {
                throw new IllegalArgumentException("Expected just Ints.");
            }

        }

        if (!collected.isEmpty()) {
            collected.trimToSize();
        }

        return unmodifiableList(collected);

    }

    private static List<Integer> flattenIntsWithRecursion(List<?> source, List<Integer> dest) {

        if (dest == null) {
            return flattenIntsWithRecursion(source, new LinkedList<>());
        }

        for (Object a : source) {
            if (a instanceof Integer) {
                dest.add((Integer) a);
            } else if (a instanceof List<?>) {
                flattenIntsWithRecursion((List<?>) a, dest);
            } else {
                throw new IllegalArgumentException("Only integers is allowed in list.");
            }
        }


        return dest;
    }

    @SafeVarargs
    private static <T> List<T> listOf(T... items) {

        List<T> collected;

        if (items.length == 0) {
            collected = Collections.emptyList();
        } else {
            collected = new ArrayList<>(items.length);
            Collections.addAll(collected, items);

        }
        return collected;
    }

    public static void main(String[] args) {

        List<Object> source = listOf(
                1, 2, 3,
                listOf(4, 5, 6),
                listOf(7, listOf(8, 9, 10)),
                listOf(100, 101));

        List<Integer> f1 = flattenInts(source);

        System.out.println(f1);

        List<Integer> f2 = flattenIntsWithRecursion(source, null);

        System.out.println(f2);

    }

}
