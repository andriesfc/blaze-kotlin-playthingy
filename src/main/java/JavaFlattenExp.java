import java.util.LinkedList;
import java.util.List;

public class JavaFlattenExp {

    public static void flattenWithOwnStack(List<?> source, List<Integer> dest) {

        LinkedList<Object> pending = new LinkedList<>();
        pending.add(source);

        while (!pending.isEmpty()) {

            Object a = pending.pop();

            if (a instanceof Integer) {
                dest.add((Integer) a);
                continue;
            }

            if (a instanceof List) {
                @SuppressWarnings("unchecked") List<Object> all = (List<Object>) a;
                for (int z = all.size(), i = 0; i < z; i++) {
                    Object x = all.get(i);
                    pending.push(x);
                }
                continue;
            }

            throw new IllegalArgumentException("Only ints and list of ints allowed.");
        }
    }

    private static void _flattenInnerRecursive(Object a, List<Integer> dest) {

        if (a instanceof Integer) {
            dest.add((Integer) a);
            return;
        }

        if (!(a instanceof List<?>)) {
            throw new IllegalArgumentException("Only ints and list of ints allowed.");
        }

        List<?> all = (List<?>) a;
        all.forEach(x -> _flattenInnerRecursive(x, dest));
    }

    public static void flattenRecursive(List<?> source, List<Integer> dest) {
        _flattenInnerRecursive(source, dest);
    }
}
